package com.mtk;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;


/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */

public abstract class ConnectionStabilizer implements java.sql.Connection {

    public final static int ATTEMPTS_COUNT = 3;
    private Connection connection;

    protected interface ConnectionChanger{
        void changeStatement() throws SQLException;
        void put(VoidSqlRunner voidSqlRunner);
    }


    public abstract class VoidSqlRunner{
        public VoidSqlRunner() throws SQLException{
            run(null);
        }
        public VoidSqlRunner(ConnectionChanger node) throws SQLException {
            node.put(this);
            run(node);
        }

        void run(ConnectionChanger node) throws SQLException {
            int  i = 0;
            while(true){
                try {
                    runSql();
                    return;
                } catch (SQLException e) {
                    if (i < ATTEMPTS_COUNT){
                        i++;
                        if (node != null) {
                            node.changeStatement();
                            return;
                        }else{
                            changeConnection();
                        }
                    }else{
                        throw e;
                    }
                }
            }
        }
        abstract void runSql() throws SQLException;
    }

    public abstract class SimpleSqlRunner<T>{
        abstract T runSql() throws SQLException;
        public T run() throws SQLException{
            return run(null);
        }
        public T run(ConnectionChanger node) throws SQLException{
            int  i = 0;
            while(true){
                try {
                    return runSql();
                } catch (SQLException e) {
                    if (i < ATTEMPTS_COUNT){
                        i++;
                        if (node != null) {
                            node.changeStatement();
                        }else{
                            changeConnection();
                        }
                    }else{
                        throw e;
                    }
                }
            }
        }
    }

    public abstract class SuperSqlRunner<T extends Statement> implements ConnectionChanger{
        Statement body;
        ConnectionChanger connectionChanger;
        SuperSqlRunner(ConnectionChanger connectionChanger) throws SQLException{
            this.connectionChanger = connectionChanger;
            fillBody();
        }
        LinkedList<VoidSqlRunner> queue;
        public void put(VoidSqlRunner voidSqlRunner){
            if (queue == null){
                queue = new LinkedList<VoidSqlRunner>();
            }
            queue.add(voidSqlRunner);
        }

        abstract Statement runSql() throws SQLException;
        public void changeStatement() throws SQLException{
            if (connectionChanger != null) {
                connectionChanger.changeStatement();
            }else{
                changeConnection();
            }
            fillBody();
        }
        public void fillBody() throws SQLException{
            int  i = 0;
            while(true){
                try {
                    body = runSql();
                    restoreBody();
                    return;
                } catch (SQLException e) {
                    if (i < ATTEMPTS_COUNT){
                        i++;
                        changeConnection();
                    }else{
                        throw e;
                    }
                }
            }
        }
        void restoreBody() throws SQLException {
            if (queue != null) {
                for (VoidSqlRunner runner : queue) {
                    runner.runSql();
                }
            }
        }
    }

    public abstract class SqlInfoSqlRunner{
        public SqlInfoSqlRunner() throws SQLClientInfoException{
            int  i = 0;
            while(true){
                try {
                    runSql();
                    return;
                } catch (SQLClientInfoException e) {
                    if (i < ATTEMPTS_COUNT){
                        i++;
                        changeConnection();
                    }else{
                        throw e;
                    }
                }
            }
        }
        abstract void runSql() throws SQLClientInfoException;
    }


    public abstract class StatementSqlRunner extends SuperSqlRunner<Statement> implements Statement{

        StatementSqlRunner() throws SQLException {
            super(null);
        }

        protected Statement getStatement(){
            return body;
        }

        @Override
        public <T> T unwrap(final Class<T> aClass) throws SQLException{
            return new SimpleSqlRunner<T>(){
                @Override
                T runSql() throws SQLException {
                    return getStatement().unwrap(aClass);
                }
            }.run(this);

        }

        @Override
        public boolean isWrapperFor(final Class<?> aClass) throws SQLException{
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().isWrapperFor(aClass);
                }
            }.run(this);

        }

        @Override
        public ResultSet executeQuery(final String s) throws SQLException{
            return new SimpleSqlRunner<ResultSet>(){
                @Override
                ResultSet runSql() throws SQLException {
                    return getStatement().executeQuery(s);
                }
            }.run(this);
        }

        @Override
        public int executeUpdate(final String s) throws SQLException{
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().executeUpdate(s);
                }
            }.run(this);
        }

        @Override
        public void close() throws SQLException{
                new VoidSqlRunner(this){
                    @Override
                    void runSql() throws SQLException{
                        getStatement().close();
                    }
                };
        }

        @Override
        public int getMaxFieldSize() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getMaxFieldSize();
                }
            }.run(this);
        }

        @Override
        public void setMaxFieldSize(final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setMaxFieldSize(i);
                }
            };
        }

        @Override
        public int getMaxRows() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getMaxRows();
                }
            }.run(this);
        }

        @Override
        public void setMaxRows(final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setMaxRows(i);
                }
            };
        }

        @Override
        public void setEscapeProcessing(final boolean b) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setEscapeProcessing(b);
                }
            };
        }

        @Override
        public int getQueryTimeout() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getQueryTimeout();
                }
            }.run(this);

        }

        @Override
        public void setQueryTimeout(final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setQueryTimeout(i);
                }
            };
        }

        @Override
        public void cancel() throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().cancel();
                }
            };
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return new SimpleSqlRunner<SQLWarning>(){
                @Override
                SQLWarning runSql() throws SQLException {
                    return getStatement().getWarnings();
                }
            }.run(this);
        }

        @Override
        public void clearWarnings() throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException{
                    getStatement().clearWarnings();
                }
            };
        }

        @Override
        public void setCursorName(final String s) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCursorName(s);
                }
            };
        }

        @Override
        public boolean execute(final String s) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().execute(s);
                }
            }.run(this);
        }

        @Override
        public ResultSet getResultSet() throws SQLException {
            return new SimpleSqlRunner<ResultSet>(){
                @Override
                ResultSet runSql() throws SQLException {
                    return getStatement().getResultSet();
                }
            }.run(this);
        }

        @Override
        public int getUpdateCount() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getUpdateCount();
                }
            }.run(this);
        }

        @Override
        public boolean getMoreResults() throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().getMoreResults();
                }
            }.run(this);

        }

        @Override
        public void setFetchDirection(final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setFetchDirection(i);
                }
            };
        }

        @Override
        public int getFetchDirection() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getFetchDirection();
                }
            }.run(this);

        }

        @Override
        public void setFetchSize(final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setFetchSize(i);
                }
            };
        }

        @Override
        public int getFetchSize() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getFetchSize();
                }
            }.run(this);

        }

        @Override
        public int getResultSetConcurrency() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getResultSetConcurrency();
                }
            }.run(this);

        }

        @Override
        public int getResultSetType() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getResultSetType();
                }
            }.run(this);

        }

        @Override
        public void addBatch(final String s) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().addBatch(s);
                }
            };
        }

        @Override
        public void clearBatch() throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().clearBatch();
                }
            };
        }

        @Override
        public int[] executeBatch() throws SQLException {
            return new SimpleSqlRunner<int[]>(){
                @Override
                int[] runSql() throws SQLException {
                    return getStatement().executeBatch();
                }
            }.run(this);
        }

        @Override
        public Connection getConnection() throws SQLException {
            return new SimpleSqlRunner<Connection>(){
                @Override
                Connection runSql() throws SQLException {
                    return getStatement().getConnection();
                }
            }.run(this);
        }

        @Override
        public boolean getMoreResults(final int i) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().getMoreResults(i);
                }
            }.run(this);
        }

        @Override
        public ResultSet getGeneratedKeys() throws SQLException {
            return new SimpleSqlRunner<ResultSet>(){
                @Override
                ResultSet runSql() throws SQLException {
                    return getStatement().getGeneratedKeys();
                }
            }.run(this);
        }

        @Override
        public int executeUpdate(final String s, final int i) throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().executeUpdate(s, i);
                }
            }.run(this);
        }

        @Override
        public int executeUpdate(final String s, final int[] ints) throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().executeUpdate(s, ints);
                }
            }.run(this);        }

        @Override
        public int executeUpdate(final String s, final String[] strings) throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().executeUpdate(s, strings);
                }
            }.run(this);
        }

        @Override
        public boolean execute(final String s, final int i) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().execute(s, i);
                }
            }.run(this);

        }

        @Override
        public boolean execute(final String s, final int[] ints) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().execute(s, ints);
                }
            }.run(this);

        }

        @Override
        public boolean execute(final String s, final String[] strings) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().execute(s, strings);
                }
            }.run(this);

        }

        @Override
        public int getResultSetHoldability() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getResultSetHoldability();
                }
            }.run(this);

        }

        @Override
        public boolean isClosed() throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().isClosed();
                }
            }.run(this);
        }

        @Override
        public void setPoolable(final boolean b) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setPoolable(b);
                }
            };
        }

        @Override
        public boolean isPoolable() throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().isPoolable();
                }
            }.run(this);
        }

        @Override
        public void closeOnCompletion() throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().closeOnCompletion();
                }
            };
        }

        @Override
        public boolean isCloseOnCompletion() throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().isCloseOnCompletion();
                }
            }.run(this);
        }
    }


    public abstract class PreparedStatementSqlRunner extends StatementSqlRunner implements PreparedStatement{

        PreparedStatementSqlRunner() throws SQLException {
            super();
        }

        protected PreparedStatement getStatement(){
            return (PreparedStatement)body;
        }

        @Override
        public ResultSet executeQuery() throws SQLException {
            return new SimpleSqlRunner<ResultSet>(){
                @Override
                ResultSet runSql() throws SQLException {
                    return getStatement().executeQuery();
                }
            }.run(this);
        }

        @Override
        public int executeUpdate() throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().executeUpdate();
                }
            }.run(this);
        }

        @Override
        public void setNull(final int i, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNull(i, i1);
                }
            };
        }

        @Override
        public void setBoolean(final int i, final boolean b) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBoolean(i, b);
                }
            };
        }

        @Override
        public void setByte(final int i, final byte b) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setByte(i, b);
                }
            };
        }

        @Override
        public void setShort(final int i, final short i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setShort(i, i1);
                }
            };
        }

        @Override
        public void setInt(final int i, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setInt(i, i1);
                }
            };
        }

        @Override
        public void setLong(final int i, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setLong(i, l);
                }
            };
        }

        @Override
        public void setFloat(final int i, final float v) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setFloat(i, v);
                }
            };
        }

        @Override
        public void setDouble(final int i, final double v) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setDouble(i, v);
                }
            };
        }

        @Override
        public void setBigDecimal(final int i, final BigDecimal bigDecimal) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBigDecimal(i, bigDecimal);
                }
            };
        }

        @Override
        public void setString(final int i, final String s) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setString(i, s);
                }
            };
        }

        @Override
        public void setBytes(final int i, final byte[] bytes) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBytes(i, bytes);
                }
            };

        }

        @Override
        public void setDate(final int i, final Date date) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setDate(i, date);
                }
            };
        }

        @Override
        public void setTime(final int i, final Time time) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTime(i, time);
                }
            };
        }

        @Override
        public void setTimestamp(final int i, final Timestamp timestamp) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTimestamp(i, timestamp);
                }
            };

        }

        @Override
        public void setAsciiStream(final int i, final InputStream inputStream, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setAsciiStream(i, inputStream, i1);                
                }
            };
        }

        @Override
        public void setUnicodeStream(final int i, final InputStream inputStream, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setUnicodeStream(i, inputStream, i1);
                }
            };
        }

        @Override
        public void setBinaryStream(final int i, final InputStream inputStream, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBinaryStream(i, inputStream, i1);
                }
            };
        }

        @Override
        public void clearParameters() throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().clearParameters();
                }
            };
        }

        @Override
        public void setObject(final int i, final Object o, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setObject(i, o, i1);
                }
            };
        }

        @Override
        public void setObject(final int i, final Object o) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setObject(i, o);
                }
            };
        }

        @Override
        public boolean execute() throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().execute();
                }
            }.run(this);
        }

        @Override
        public void addBatch() throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().addBatch();
                }
            };
        }

        @Override
        public void setCharacterStream(final int i, final Reader reader, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCharacterStream(i, reader, i1);
                }
            };
        }

        @Override
        public void setRef(final int i, final Ref ref) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setRef(i, ref);
                }
            };
        }

        @Override
        public void setBlob(final int i, final Blob blob) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBlob(i, blob);
                }
            };
        }

        @Override
        public void setClob(final int i, final Clob clob) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setClob(i, clob);
                }
            };
        }

        @Override
        public void setArray(final int i, final Array array) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setArray(i, array);
                }
            };
        }

        @Override
        public ResultSetMetaData getMetaData() throws SQLException {
            return new SimpleSqlRunner<ResultSetMetaData>(){
                @Override
                ResultSetMetaData runSql() throws SQLException {
                    return getStatement().getMetaData();
                }
            }.run(this);
        }

        @Override
        public void setDate(final int i, final Date date, final Calendar calendar) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setDate(i, date, calendar);
                }
            };

        }

        @Override
        public void setTime(final int i, final Time time, final Calendar calendar) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTime(i, time, calendar);
                }
            };

        }

        @Override
        public void setTimestamp(final int i, final Timestamp timestamp, final Calendar calendar) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTimestamp(i, timestamp, calendar);
                }
            };
        }

        @Override
        public void setNull(final int i, final int i1, final String s) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNull(i, i1, s);
                }
            };
        }

        @Override
        public void setURL(final int i, final URL url) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setURL(i, url);
                }
            };
        }

        @Override
        public ParameterMetaData getParameterMetaData() throws SQLException {
            return new SimpleSqlRunner<ParameterMetaData>(){
                @Override
                ParameterMetaData runSql() throws SQLException {
                    return getStatement().getParameterMetaData();
                }
            }.run(this);
        }

        @Override
        public void setRowId(final int i, final RowId rowId) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setRowId(i, rowId);
                }
            };
        }

        @Override
        public void setNString(final int i, final String s) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNString(i, s);
                }
            };
        }

        @Override
        public void setNCharacterStream(final int i, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNCharacterStream(i, reader, l);
                }
            };
        }

        @Override
        public void setNClob(final int i, final NClob nClob) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNClob(i, nClob);
                }
            };
        }

        @Override
        public void setClob(final int i, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setClob(i, reader, l);
                }
            };
        }

        @Override
        public void setBlob(final int i, final InputStream inputStream, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBlob(i, inputStream, l);
                }
            };
        }

        @Override
        public void setNClob(final int i, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNClob(i, reader, l);
                }
            };
        }

        @Override
        public void setSQLXML(final int i, final SQLXML sqlxml) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setSQLXML(i, sqlxml);
                }
            };
        }

        @Override
        public void setObject(final int i, final Object o, final int i1, final int i2) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setObject(i, o, i1, i2);
                }
            };
        }

        @Override
        public void setAsciiStream(final int i, final InputStream inputStream, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setAsciiStream(i, inputStream, l);
                }
            };
        }

        @Override
        public void setBinaryStream(final int i, final InputStream inputStream, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBinaryStream(i, inputStream, l);
                }
            };
        }

        @Override
        public void setCharacterStream(final int i, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCharacterStream(i, reader, l);
                }
            };
        }

        @Override
        public void setAsciiStream(final int i, final InputStream inputStream) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setAsciiStream(i, inputStream);
                }
            };
        }

        @Override
        public void setBinaryStream(final int i, final InputStream inputStream) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBinaryStream(i, inputStream);
                }
            };
        }

        @Override
        public void setCharacterStream(final int i, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCharacterStream(i, reader);
                }
            };
        }

        @Override
        public void setNCharacterStream(final int i, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNCharacterStream(i, reader);
                }
            };
        }

        @Override
        public void setClob(final int i, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setClob(i, reader);
                }
            };
        }

        @Override
        public void setBlob(final int i, final InputStream inputStream) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBlob(i, inputStream);
                }
            };
        }

        @Override
        public void setNClob(final int i, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNClob(i, reader);
                }
            };
        }

    }


    public abstract class CallableStatementSqlRunner extends PreparedStatementSqlRunner implements CallableStatement {

        CallableStatementSqlRunner() throws SQLException {
            super();
        }

        protected CallableStatement getStatement(){
            return (CallableStatement)body;
        }

        @Override
        public void registerOutParameter(final int i, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().registerOutParameter(i, i1);
                }
            };
        }

        @Override
        public void registerOutParameter(final int i, final int i1, final int i2) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().registerOutParameter(i, i1, i2);
                }
            };
        }

        @Override
        public boolean wasNull() throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().wasNull();
                }
            }.run(this);
        }

        @Override
        public String getString(final int i) throws SQLException {
            return new SimpleSqlRunner<String>(){
                @Override
                String runSql() throws SQLException {
                    return getStatement().getString(i);
                }
            }.run(this);
        }

        @Override
        public boolean getBoolean(final int i) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().getBoolean(i);
                }
            }.run(this);

        }

        @Override
        public byte getByte(final int i) throws SQLException {
            return new SimpleSqlRunner<Byte>(){
                @Override
                Byte runSql() throws SQLException {
                    return getStatement().getByte(i);
                }
            }.run(this);
        }

        @Override
        public short getShort(final int i) throws SQLException {
            return new SimpleSqlRunner<Short>(){
                @Override
                Short runSql() throws SQLException {
                    return getStatement().getShort(i);
                }
            }.run(this);
        }

        @Override
        public int getInt(final int i) throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getInt(i);
                }
            }.run(this);
        }

        @Override
        public long getLong(final int i) throws SQLException {
            return new SimpleSqlRunner<Long>(){
                @Override
                Long runSql() throws SQLException {
                    return getStatement().getLong(i);
                }
            }.run(this);
        }

        @Override
        public float getFloat(final int i) throws SQLException {
            return new SimpleSqlRunner<Float>(){
                @Override
                Float runSql() throws SQLException {
                    return getStatement().getFloat(i);
                }
            }.run(this);
        }

        @Override
        public double getDouble(final int i) throws SQLException {
            return new SimpleSqlRunner<Double>(){
                @Override
                Double runSql() throws SQLException {
                    return getStatement().getDouble(i);
                }
            }.run(this);
        }

        @Override
        public BigDecimal getBigDecimal(final int i, final int i1) throws SQLException {
            return new SimpleSqlRunner<BigDecimal>(){
                @Override
                BigDecimal runSql() throws SQLException {
                    return getStatement().getBigDecimal(i, i1);
                }
            }.run(this);
        }

        @Override
        public byte[] getBytes(final int i) throws SQLException {
            return new SimpleSqlRunner<byte[]>(){
                @Override
                byte[] runSql() throws SQLException {
                    return getStatement().getBytes(i);
                }
            }.run(this);
        }

        @Override
        public Date getDate(final int i) throws SQLException {
            return new SimpleSqlRunner<Date>(){
                @Override
                Date runSql() throws SQLException {
                    return getStatement().getDate(i);
                }
            }.run(this);
        }

        @Override
        public Time getTime(final int i) throws SQLException {
            return new SimpleSqlRunner<Time>(){
                @Override
                Time runSql() throws SQLException {
                    return getStatement().getTime(i);
                }
            }.run(this);
        }

        @Override
        public Timestamp getTimestamp(final int i) throws SQLException {
            return new SimpleSqlRunner<Timestamp>(){
                @Override
                Timestamp runSql() throws SQLException {
                    return getStatement().getTimestamp(i);
                }
            }.run(this);
        }

        @Override
        public Object getObject(final int i) throws SQLException {
            return new SimpleSqlRunner<Object>(){
                @Override
                Object runSql() throws SQLException {
                    return getStatement().getObject(i);
                }
            }.run(this);
        }

        @Override
        public BigDecimal getBigDecimal(final int i) throws SQLException {
            return new SimpleSqlRunner<BigDecimal>(){
                @Override
                BigDecimal runSql() throws SQLException {
                    return getStatement().getBigDecimal(i);
                }
            }.run(this);

        }

        @Override
        public Object getObject(final int i, final Map<String,  Class<?>> map) throws SQLException {
            return new SimpleSqlRunner<Object>(){
                @Override
                Object runSql() throws SQLException {
                    return getStatement().getObject(i, map);
                }
            }.run(this);

        }

        @Override
        public Ref getRef(final int i) throws SQLException {
            return new SimpleSqlRunner<Ref>(){
                @Override
                Ref runSql() throws SQLException {
                    return getStatement().getRef(i);
                }
            }.run(this);
        }

        @Override
        public Blob getBlob(final int i) throws SQLException {
            return new SimpleSqlRunner<Blob>(){
                @Override
                Blob runSql() throws SQLException {
                    return getStatement().getBlob(i);
                }
            }.run(this);

        }

        @Override
        public Clob getClob(final int i) throws SQLException {
            return new SimpleSqlRunner<Clob>(){
                @Override
                Clob runSql() throws SQLException {
                    return getStatement().getClob(i);
                }
            }.run(this);

        }

        @Override
        public Array getArray(final int i) throws SQLException {
            return new SimpleSqlRunner<Array>(){
                @Override
                Array runSql() throws SQLException {
                    return getStatement().getArray(i);
                }
            }.run(this);
        }

        @Override
        public Date getDate(final int i, final Calendar calendar) throws SQLException {
            return new SimpleSqlRunner<Date>(){
                @Override
                Date runSql() throws SQLException {
                    return getStatement().getDate(i, calendar);
                }
            }.run(this);

        }

        @Override
        public Time getTime(final int i, final Calendar calendar) throws SQLException {
            return new SimpleSqlRunner<Time>(){
                @Override
                Time runSql() throws SQLException {
                    return getStatement().getTime(i, calendar);
                }
            }.run(this);

        }

        @Override
        public Timestamp getTimestamp(final int i, final Calendar calendar) throws SQLException {
            return new SimpleSqlRunner<Timestamp>(){
                @Override
                Timestamp runSql() throws SQLException {
                    return getStatement().getTimestamp(i, calendar);
                }
            }.run(this);

        }

        @Override
        public void registerOutParameter(final int i, final int i1, final String s) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().registerOutParameter(i, i1, s);
                }
            };
        }

        @Override
        public void registerOutParameter(final String s, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().registerOutParameter(s, i);
                }
            };
        }

        @Override
        public void registerOutParameter(final String s, final int i, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().registerOutParameter(s, i, i1);
                }
            };
        }

        @Override
        public void registerOutParameter(final String s, final int i, final String s1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().registerOutParameter(s, i, s1);
                }
            };
        }

        @Override
        public URL getURL(final int i) throws SQLException {
            return new SimpleSqlRunner<URL>(){
                @Override
                URL runSql() throws SQLException {
                    return getStatement().getURL(i);
                }
            }.run(this);
        }

        @Override
        public void setURL(final String s, final URL url) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setURL(s, url);
                }
            };
        }

        @Override
        public void setNull(final String s, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNull(s, i);
                }
            };
        }

        @Override
        public void setBoolean(final String s, final boolean b) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBoolean(s, b);
                }
            };
        }

        @Override
        public void setByte(final String s, final byte b) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setByte(s, b);
                }
            };
        }

        @Override
        public void setShort(final String s, final short i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setShort(s, i);
                }
            };
        }

        @Override
        public void setInt(final String s, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setInt(s, i);
                }
            };
        }

        @Override
        public void setLong(final String s, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setLong(s, l);
                }
            };
        }

        @Override
        public void setFloat(final String s, final float v) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setFloat(s, v);
                }
            };
        }

        @Override
        public void setDouble(final String s, final double v) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setDouble(s, v);
                }
            };
        }

        @Override
        public void setBigDecimal(final String s, final BigDecimal bigDecimal) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBigDecimal(s, bigDecimal);
                }
            };
        }

        @Override
        public void setString(final String s, final String s1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setString(s, s1);
                }
            };
        }

        @Override
        public void setBytes(final String s, final byte[] bytes) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBytes(s, bytes);
                }
            };
        }

        @Override
        public void setDate(final String s, final Date date) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setDate(s, date);
                }
            };
        }

        @Override
        public void setTime(final String s, final Time time) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTime(s, time);
                }
            };
        }

        @Override
        public void setTimestamp(final String s, final Timestamp timestamp) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTimestamp(s, timestamp);
                }
            };
        }

        @Override
        public void setAsciiStream(final String s, final InputStream inputStream, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setAsciiStream(s, inputStream, i);
                }
            };
        }

        @Override
        public void setBinaryStream(final String s, final InputStream inputStream, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBinaryStream(i, inputStream, i);
                }
            };
        }

        @Override
        public void setObject(final String s, final Object o, final int i, final int i1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setObject(s, o, i, i1);
                }
            };
        }

        @Override
        public void setObject(final String s, final Object o, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setObject(s, o, i);
                }
            };
        }

        @Override
        public void setObject(final String s, final Object o) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setObject(s, o);
                }
            };
        }

        @Override
        public void setCharacterStream(final String s, final Reader reader, final int i) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCharacterStream(s, reader, i);
                }
            };
        }

        @Override
        public void setDate(final String s, final Date date, final Calendar calendar) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setDate(s, date, calendar);
                }
            };
        }

        @Override
        public void setTime(final String s, final Time time, final Calendar calendar) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTime(s, time, calendar);
                }
            };
        }

        @Override
        public void setTimestamp(final String s, final Timestamp timestamp, final Calendar calendar) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setTimestamp(s, timestamp, calendar);
                }
            };
        }

        @Override
        public void setNull(final String s, final int i, final String s1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNull(s, i, s1);
                }
            };
        }

        @Override
        public String getString(final String s) throws SQLException {
            return new SimpleSqlRunner<String>(){
                @Override
                String runSql() throws SQLException {
                    return getStatement().getString(s);
                }
            }.run(this);

        }

        @Override
        public boolean getBoolean(final String s) throws SQLException {
            return new SimpleSqlRunner<Boolean>(){
                @Override
                Boolean runSql() throws SQLException {
                    return getStatement().getBoolean(s);
                }
            }.run(this);

        }

        @Override
        public byte getByte(final String s) throws SQLException {
            return new SimpleSqlRunner<Byte>(){
                @Override
                Byte runSql() throws SQLException {
                    return getStatement().getByte(s);
                }
            }.run(this);
        }

        @Override
        public short getShort(final String s) throws SQLException {
            return new SimpleSqlRunner<Short>(){
                @Override
                Short runSql() throws SQLException {
                    return getStatement().getShort(s);
                }
            }.run(this);
        }

        @Override
        public int getInt(final String s) throws SQLException {
            return new SimpleSqlRunner<Integer>(){
                @Override
                Integer runSql() throws SQLException {
                    return getStatement().getInt(s);
                }
            }.run(this);
        }

        @Override
        public long getLong(final String s) throws SQLException {
            return new SimpleSqlRunner<Long>(){
                @Override
                Long runSql() throws SQLException {
                    return getStatement().getLong(s);
                }
            }.run(this);
        }

        @Override
        public float getFloat(final String s) throws SQLException {
            return new SimpleSqlRunner<Float>(){
                @Override
                Float runSql() throws SQLException {
                    return getStatement().getFloat(s);
                }
            }.run(this);
        }

        @Override
        public double getDouble(final String s) throws SQLException {
            return new SimpleSqlRunner<Double>(){
                @Override
                Double runSql() throws SQLException {
                    return getStatement().getDouble(s);
                }
            }.run(this);
        }

        @Override
        public byte[] getBytes(final String s) throws SQLException {
            return new SimpleSqlRunner<byte[]>(){
                @Override
                byte[] runSql() throws SQLException {
                    return getStatement().getBytes(s);
                }
            }.run(this);

        }

        @Override
        public Date getDate(final String s) throws SQLException {
            return new SimpleSqlRunner<Date>(){
                @Override
                Date runSql() throws SQLException {
                    return getStatement().getDate(s);
                }
            }.run(this);

        }

        @Override
        public Time getTime(final String s) throws SQLException {
            return new SimpleSqlRunner<Time>(){
                @Override
                Time runSql() throws SQLException {
                    return getStatement().getTime(s);
                }
            }.run(this);

        }

        @Override
        public Timestamp getTimestamp(final String s) throws SQLException {
            return new SimpleSqlRunner<Timestamp>(){
                @Override
                Timestamp runSql() throws SQLException {
                    return getStatement().getTimestamp(s);
                }
            }.run(this);

        }

        @Override
        public Object getObject(final String s) throws SQLException {
            return new SimpleSqlRunner<Object>(){
                @Override
                Object runSql() throws SQLException {
                    return getStatement().getObject(s);
                }
            }.run(this);

        }

        @Override
        public BigDecimal getBigDecimal(final String s) throws SQLException {
            return new SimpleSqlRunner<BigDecimal>(){
                @Override
                BigDecimal runSql() throws SQLException {
                    return getStatement().getBigDecimal(s);
                }
            }.run(this);

        }

        @Override
        public Object getObject(final String s, final Map<String,  Class<?>> map) throws SQLException {
            return new SimpleSqlRunner<Object>(){
                @Override
                Object runSql() throws SQLException {
                    return getStatement().getObject(s, map);
                }
            }.run(this);

        }

        @Override
        public Ref getRef(final String s) throws SQLException {
            return new SimpleSqlRunner<Ref>(){
                @Override
                Ref runSql() throws SQLException {
                    return getStatement().getRef(s);
                }
            }.run(this);
        }

        @Override
        public Blob getBlob(final String s) throws SQLException {
            return new SimpleSqlRunner<Blob>(){
                @Override
                Blob runSql() throws SQLException {
                    return getStatement().getBlob(s);
                }
            }.run(this);
        }

        @Override
        public Clob getClob(final String s) throws SQLException {
            return new SimpleSqlRunner<Clob>(){
                @Override
                Clob runSql() throws SQLException {
                    return getStatement().getClob(s);
                }
            }.run(this);
        }

        @Override
        public Array getArray(final String s) throws SQLException {
            return new SimpleSqlRunner<Array>(){
                @Override
                Array runSql() throws SQLException {
                    return getStatement().getArray(s);
                }
            }.run(this);
        }

        @Override
        public Date getDate(final String s, final Calendar calendar) throws SQLException {
            return new SimpleSqlRunner<Date>(){
                @Override
                Date runSql() throws SQLException {
                    return getStatement().getDate(s, calendar);
                }
            }.run(this);

        }

        @Override
        public Time getTime(final String s, final Calendar calendar) throws SQLException {
            return new SimpleSqlRunner<Time>(){
                @Override
                Time runSql() throws SQLException {
                    return getStatement().getTime(s, calendar);
                }
            }.run(this);

        }

        @Override
        public Timestamp getTimestamp(final String s, final Calendar calendar) throws SQLException {
            return new SimpleSqlRunner<Timestamp>(){
                @Override
                Timestamp runSql() throws SQLException {
                    return getStatement().getTimestamp(s, calendar);
                }
            }.run(this);

        }

        @Override
        public URL getURL(final String s) throws SQLException {
            return new SimpleSqlRunner<URL>(){
                @Override
                URL runSql() throws SQLException {
                    return getStatement().getURL(s);
                }
            }.run(this);
        }

        @Override
        public RowId getRowId(final int i) throws SQLException {
            return new SimpleSqlRunner<RowId>(){
                @Override
                RowId runSql() throws SQLException {
                    return getStatement().getRowId(i);
                }
            }.run(this);
        }

        @Override
        public RowId getRowId(final String s) throws SQLException {
            return new SimpleSqlRunner<RowId>(){
                @Override
                RowId runSql() throws SQLException {
                    return getStatement().getRowId(s);
                }
            }.run(this);
        }

        @Override
        public void setRowId(final String s, final RowId rowId) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setRowId(s, rowId);
                }
            };
        }

        @Override
        public void setNString(final String s, final String s1) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNString(s, s1);
                }
            };
        }

        @Override
        public void setNCharacterStream(final String s, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNCharacterStream(s, reader, l);
                }
            };
        }

        @Override
        public void setNClob(final String s, final NClob nClob) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNClob(s, nClob);
                }
            };
        }

        @Override
        public void setClob(final String s, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setClob(s, reader, l);
                }
            };
        }

        @Override
        public void setBlob(final String s, final InputStream inputStream, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBlob(s, inputStream, l);
                }
            };
        }

        @Override
        public void setNClob(final String s, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNClob(s, reader, l);
                }
            };
        }

        @Override
        public NClob getNClob(final int i) throws SQLException {
            return new SimpleSqlRunner<NClob>(){
                @Override
                NClob runSql() throws SQLException {
                    return getStatement().getNClob(i);
                }
            }.run(this);
        }

        @Override
        public NClob getNClob(final String s) throws SQLException {
            return new SimpleSqlRunner<NClob>(){
                @Override
                NClob runSql() throws SQLException {
                    return getStatement().getNClob(s);
                }
            }.run(this);
        }

        @Override
        public void setSQLXML(final String s, final SQLXML sqlxml) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setSQLXML(s, sqlxml);
                }
            };
        }

        @Override
        public SQLXML getSQLXML(final int i) throws SQLException {
            return new SimpleSqlRunner<SQLXML>(){
                @Override
                SQLXML runSql() throws SQLException {
                    return getStatement().getSQLXML(i);
                }
            }.run(this);
        }

        @Override
        public SQLXML getSQLXML(final String s) throws SQLException {
            return new SimpleSqlRunner<SQLXML>(){
                @Override
                SQLXML runSql() throws SQLException {
                    return getStatement().getSQLXML(s);
                }
            }.run(this);
        }

        @Override
        public String getNString(final int i) throws SQLException {
            return new SimpleSqlRunner<String>(){
                @Override
                String runSql() throws SQLException {
                    return getStatement().getNString(i);
                }
            }.run(this);

        }

        @Override
        public String getNString(final String s) throws SQLException {
            return new SimpleSqlRunner<String>(){
                @Override
                String runSql() throws SQLException {
                    return getStatement().getNString(s);
                }
            }.run(this);
        }

        @Override
        public Reader getNCharacterStream(final int i) throws SQLException {
            return new SimpleSqlRunner<Reader>(){
                @Override
                Reader runSql() throws SQLException {
                    return getStatement().getNCharacterStream(i);
                }
            }.run(this);
        }

        @Override
        public Reader getNCharacterStream(final String s) throws SQLException {
            return new SimpleSqlRunner<Reader>(){
                @Override
                Reader runSql() throws SQLException {
                    return getStatement().getNCharacterStream(s);
                }
            }.run(this);
        }

        @Override
        public Reader getCharacterStream(final int i) throws SQLException {
            return new SimpleSqlRunner<Reader>(){
                @Override
                Reader runSql() throws SQLException {
                    return getStatement().getCharacterStream(i);
                }
            }.run(this);
        }

        @Override
        public Reader getCharacterStream(final String s) throws SQLException {
            return new SimpleSqlRunner<Reader>(){
                @Override
                Reader runSql() throws SQLException {
                    return getStatement().getCharacterStream(s);
                }
            }.run(this);
        }

        @Override
        public void setBlob(final String s, final Blob blob) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBlob(s, blob);
                }
            };
        }

        @Override
        public void setClob(final String s, final Clob clob) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setClob(s, clob);
                }
            };
        }

        @Override
        public void setAsciiStream(final String s, final InputStream inputStream, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setAsciiStream(s, inputStream, l);
                }
            };
        }

        @Override
        public void setBinaryStream(final String s, final InputStream inputStream, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBinaryStream(s, inputStream, l);
                }
            };
        }

        @Override
        public void setCharacterStream(final String s, final Reader reader, final long l) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCharacterStream(s, reader, l);
                }
            };
        }

        @Override
        public void setAsciiStream(final String s, final InputStream inputStream) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setAsciiStream(s, inputStream);
                }
            };
        }

        @Override
        public void setBinaryStream(final String s, final InputStream inputStream) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBinaryStream(s, inputStream);
                }
            };
        }

        @Override
        public void setCharacterStream(final String s, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setCharacterStream(s, reader);
                }
            };
        }

        @Override
        public void setNCharacterStream(final String s, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNCharacterStream(s, reader);
                }
            };
        }

        @Override
        public void setClob(final String s, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setClob(s, reader);
                }
            };
        }

        @Override
        public void setBlob(final String s, final InputStream inputStream) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setBlob(s, inputStream);
                }
            };
        }

        @Override
        public void setNClob(final String s, final Reader reader) throws SQLException {
            new VoidSqlRunner(this){
                @Override
                void runSql() throws SQLException {
                    getStatement().setNClob(s, reader);
                }
            };
        }

        @Override
        public <T> T getObject(final int i, final Class<T> aClass) throws SQLException {
            return new SimpleSqlRunner<T>(){
                @Override
                T runSql() throws SQLException {
                    return getStatement().getObject(i, aClass);
                }
            }.run(this);
        }

        @Override
        public <T> T getObject(final String s, final Class<T> aClass) throws SQLException {
            return new SimpleSqlRunner<T>(){
                @Override
                T runSql() throws SQLException {
                    return getStatement().getObject(s, aClass);
                }
            }.run(this);
        }
    }

    @Override
    public Statement createStatement() throws SQLException {
        return new StatementSqlRunner(){
            @Override
            Statement runSql() throws SQLException {
                return connection.createStatement();
            }
        };
    }

    @Override
    public PreparedStatement prepareStatement(final String s) throws SQLException {
        return new PreparedStatementSqlRunner(){
            @Override
            PreparedStatement runSql() throws SQLException {
                return connection.prepareStatement(s);
            }
        };
    }


    @Override
    public CallableStatement prepareCall(final String s) throws SQLException {
        return new CallableStatementSqlRunner(){
            @Override
            CallableStatement runSql() throws SQLException {
                return connection.prepareCall(s);
            }
        };
    }


    @Override
    public String nativeSQL(final String s) throws SQLException {
        return new SimpleSqlRunner<String>(){
            @Override
            String runSql() throws SQLException {
                return connection.nativeSQL(s);
            }
        }.run();
    }

    @Override
    public void setAutoCommit(final boolean b) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setAutoCommit(b);
            }
        };
    }

    @Override
    public boolean getAutoCommit() throws SQLException {
        return new SimpleSqlRunner<Boolean>(){
            @Override
            Boolean runSql() throws SQLException {
                return connection.getAutoCommit();
            }
        }.run();
    }

    @Override
    public void commit() throws SQLException {
        new VoidSqlRunner() {
            @Override
            void runSql() throws SQLException {
                connection.commit();
            }
        };
    }

    @Override
    public void rollback() throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.rollback();
            }
        };

    }

    @Override
    public void close() throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.close();
            }
        };
    }

    @Override
    public boolean isClosed() throws SQLException {
        return new SimpleSqlRunner<Boolean>(){
            @Override
            Boolean runSql() throws SQLException {
                return connection.isClosed();
            }
        }.run();
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        return new SimpleSqlRunner<DatabaseMetaData>(){
            @Override
            DatabaseMetaData runSql() throws SQLException {
                return connection.getMetaData();
            }
        }.run();
    }

    @Override
    public void setReadOnly(final boolean b) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setReadOnly(b);
            }
        };
    }

    @Override
    public boolean isReadOnly() throws SQLException {
        return new SimpleSqlRunner<Boolean>(){
            @Override
            Boolean runSql() throws SQLException {
                return connection.isReadOnly();
            }
        }.run();

    }

    @Override
    public void setCatalog(final String s) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setCatalog(s);
            }
        };
    }

    @Override
    public String getCatalog() throws SQLException {
        return new SimpleSqlRunner<String>(){
            @Override
            String runSql() throws SQLException {
                return connection.getCatalog();
            }
        }.run();
    }

    @Override
    public void setTransactionIsolation(final int i) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setTransactionIsolation(i);
            }
        };
    }

    @Override
    public int getTransactionIsolation() throws SQLException {
        return new SimpleSqlRunner<Integer>(){
            @Override
            Integer runSql() throws SQLException {
                return connection.getTransactionIsolation();
            }
        }.run();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        return new SimpleSqlRunner<SQLWarning>(){
            @Override
            SQLWarning runSql() throws SQLException {
                return connection.getWarnings();
            }
        }.run();
    }

    @Override
    public void clearWarnings() throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.clearWarnings();
            }
        };
    }

    @Override
    public Statement createStatement(final int i, final int i1) throws SQLException {
        return new StatementSqlRunner(){
            @Override
            Statement runSql() throws SQLException {
                return connection.createStatement(i, i1);
            }
        };

    }

    @Override
    public PreparedStatement prepareStatement(final String s, final int i, final int i1) throws SQLException {
        return new PreparedStatementSqlRunner(){
            @Override
            PreparedStatement runSql() throws SQLException {
                return connection.prepareStatement(s, i, i1);
            }
        };
    }

    @Override
    public CallableStatement prepareCall(final String s, final int i, final int i1) throws SQLException {
        return new CallableStatementSqlRunner(){
            @Override
            CallableStatement runSql() throws SQLException {
                return connection.prepareCall(s, i, i1);
            }
        };
    }

    @Override
    public Map<String,  Class<?>> getTypeMap() throws SQLException {
        return new SimpleSqlRunner<Map<String,  Class<?>>>(){
            @Override
            Map<String,  Class<?>> runSql() throws SQLException {
                return connection.getTypeMap();
            }
        }.run();
    }

    @Override
    public void setTypeMap(final Map<String,  Class<?>> map) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setTypeMap(map);
            }
        };
    }

    @Override
    public void setHoldability(final int i) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setHoldability(i);
            }
        };
    }

    @Override
    public int getHoldability() throws SQLException {
        return new SimpleSqlRunner<Integer>(){
            @Override
            Integer runSql() throws SQLException {
                return connection.getHoldability();
            }
        }.run();

    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
        return new SimpleSqlRunner<Savepoint>(){
            @Override
            Savepoint runSql() throws SQLException {
                return connection.setSavepoint();
            }
        }.run();
    }

    @Override
    public Savepoint setSavepoint(final String s) throws SQLException {
        return new SimpleSqlRunner<Savepoint>(){
            @Override
            Savepoint runSql() throws SQLException {
                return connection.setSavepoint(s);
            }
        }.run();
    }

    @Override
    public void rollback(final Savepoint savepoint) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.rollback(savepoint);
            }
        };
    }

    @Override
    public void releaseSavepoint(final Savepoint savepoint) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.releaseSavepoint(savepoint);
            }
        };
    }

    @Override
    public Statement createStatement(final int i, final int i1, final int i2) throws SQLException {
        return new StatementSqlRunner(){
            @Override
            Statement runSql() throws SQLException {
                return connection.createStatement(i, i1, i2);
            }
        };
    }

    @Override
    public PreparedStatement prepareStatement(final String s, final int i, final int i1, final int i2) throws SQLException {
        return new PreparedStatementSqlRunner(){
            @Override
            PreparedStatement runSql() throws SQLException {
                return connection.prepareStatement(s, i, i1, i2);
            }
        };
    }

    @Override
    public CallableStatement prepareCall(final String s, final int i, final int i1, final int i2) throws SQLException {
        return new CallableStatementSqlRunner(){
            @Override
            CallableStatement runSql() throws SQLException {
                return connection.prepareCall(s, i, i1, i2);
            }
        };
    }

    @Override
    public PreparedStatement prepareStatement(final String s, final int i) throws SQLException {
        return new PreparedStatementSqlRunner(){
            @Override
            PreparedStatement runSql() throws SQLException {
                return connection.prepareStatement(s, i);
            }
        };
    }

    @Override
    public PreparedStatement prepareStatement(final String s, final int[] ints) throws SQLException {
        return new PreparedStatementSqlRunner(){
            @Override
            PreparedStatement runSql() throws SQLException {
                return connection.prepareStatement(s, ints);
            }
        };
    }

    @Override
    public PreparedStatement prepareStatement(final String s, final String[] strings) throws SQLException {
        return new PreparedStatementSqlRunner(){
            @Override
            PreparedStatement runSql() throws SQLException {
                return connection.prepareStatement(s, strings);
            }
        };
    }

    @Override
    public Clob createClob() throws SQLException {
        return new SimpleSqlRunner<Clob>(){
            @Override
            Clob runSql() throws SQLException {
                return connection.createClob();
            }
        }.run();
    }

    @Override
    public Blob createBlob() throws SQLException {
        return new SimpleSqlRunner<Blob>(){
            @Override
            Blob runSql() throws SQLException {
                return connection.createBlob();
            }
        }.run();
    }

    @Override
    public NClob createNClob() throws SQLException {
        return new SimpleSqlRunner<NClob>(){
            @Override
            NClob runSql() throws SQLException {
                return connection.createNClob();
            }
        }.run();
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        return new SimpleSqlRunner<SQLXML>(){
            @Override
            SQLXML runSql() throws SQLException {
                return connection.createSQLXML();
            }
        }.run();
    }

    @Override
    public boolean isValid(final int i) throws SQLException {
        return new SimpleSqlRunner<Boolean>(){
            @Override
            Boolean runSql() throws SQLException {
                return connection.isValid(i);
            }
        }.run();
    }

    @Override
    public void setClientInfo(final String s, final String s1) throws SQLClientInfoException {
        new SqlInfoSqlRunner(){
            @Override
            void runSql() throws SQLClientInfoException {
                connection.setClientInfo(s, s1);
            }
        };
    }

    @Override
    public void setClientInfo(final Properties properties) throws SQLClientInfoException {
        new SqlInfoSqlRunner(){
            @Override
            void runSql() throws SQLClientInfoException {
                connection.setClientInfo(properties);
            }
        };
    }

    @Override
    public String getClientInfo(final String s) throws SQLException {
        return new SimpleSqlRunner<String>(){
            @Override
            String runSql() throws SQLException {
                return connection.getClientInfo(s);
            }
        }.run();
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        return new SimpleSqlRunner<Properties>(){
            @Override
            Properties runSql() throws SQLException {
                return connection.getClientInfo();
            }
        }.run();

    }

    @Override
    public Array createArrayOf(final String s, final Object[] objects) throws SQLException {
        return new SimpleSqlRunner<Array>(){
            @Override
            Array runSql() throws SQLException {
                return connection.createArrayOf(s, objects);
            }
        }.run();
    }

    @Override
    public Struct createStruct(final String s, final Object[] objects) throws SQLException {
        return new SimpleSqlRunner<Struct>(){
            @Override
            Struct runSql() throws SQLException {
                return connection.createStruct(s, objects);
            }
        }.run();
    }

    @Override
    public void setSchema(final String s) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setSchema(s);
            }
        };
    }

    @Override
    public String getSchema() throws SQLException {
        return new SimpleSqlRunner<String>(){
            @Override
            String runSql() throws SQLException {
                return connection.getSchema();
            }
        }.run();
    }

    @Override
    public void abort(final Executor executor) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.abort(executor);
            }
        };
    }

    @Override
    public void setNetworkTimeout(final Executor executor, final int i) throws SQLException {
        new VoidSqlRunner(){
            @Override
            void runSql() throws SQLException {
                connection.setNetworkTimeout(executor, i);
            }
        };
    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        return new SimpleSqlRunner<Integer>(){
            @Override
            Integer runSql() throws SQLException {
                return connection.getNetworkTimeout();
            }
        }.run();
    }

    @Override
    public <T> T unwrap(final Class<T> aClass) throws SQLException {
        return new SimpleSqlRunner<T>(){
            @Override
            T runSql() throws SQLException {
                return connection.unwrap(aClass);
            }
        }.run();
    }

    @Override
    public boolean isWrapperFor(final Class<?> aClass) throws SQLException {
        return new SimpleSqlRunner<Boolean>(){
            @Override
            Boolean runSql() throws SQLException {
                return connection.isWrapperFor(aClass);
            }
        }.run();

    }

    public abstract Connection reconnect();
    
    public ConnectionStabilizer(){
        changeConnection();
    }

    public void changeConnection(){
        connection = reconnect();
    }

}
